class Output
{
    public float value=0;
    Brain brain;
    public Output(Brain a)
    {
        brain=a;
    }
    public void draw()
    {
        int i=brain.outputs.indexOf(this);
        fill(#000000);
        noStroke();
        ellipse(32, 20+i*20, 50, 18);
        fill(#ffffff);
        text(value, 10, 25+i*20);
    }

    public void setBrain(Brain a)
    {
        brain=a;
    }
}