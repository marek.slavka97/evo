class Neuron
{
    Brain brain;
    ArrayList<Output> inputs;
    boolean[] gates;
    float[] weights;
    Output output;
    float MOD;
    public Neuron(Brain a, ArrayList<Output> i)
    {
        brain=a;
        inputs=i;
        output=new Output(a);
        inputs.add(output);
    }
    
    public Neuron copy(Brain newBrain, ArrayList<Output> newOutputs)
    {
        Neuron copy=new Neuron(newBrain,newOutputs);
        return copy;
    }

    public void copyGates(Neuron template)
    {
        if(template.gates.length<=gates.length)
        {
        for(int i=0;i<template.gates.length;i++)
            if(random(1)>0.1)
            {
                gates[i]=template.gates[i];
            }
        for(int i=0;i<template.weights.length;i++)
            weights[i]=template.weights[i]+random(0.2)-0.1;
        }
        else
        {
        for(int i=0;i<gates.length;i++)
            if(random(1)>0.05)
            {
                gates[i]=template.gates[i];
            }
        for(int i=0;i<weights.length;i++)
            weights[i]=template.weights[i]+random(0.2)-0.1;
        }
        
        MOD=template.MOD+random(0.2)-0.1;
    }

    public void initGates()
    {
        gates=new boolean[inputs.size()];
        for(int i=0;i<gates.length;i++)gates[i]=(random(1) > 0.5);
        weights=new float[inputs.size()];
        for(int i=0;i<weights.length;i++)weights[i]=random(4)-2;
        MOD=random(4)-2;
    }

    public void step()
    {
        float temp=0;
        for(int i=0;i<inputs.size();i++)
        {
            if(gates[i])
            {
                temp+=inputs.get(i).value*weights[i];
            }
        }
        output.value=atan(temp*MOD)/HALF_PI;
    }

    public float getGates()
    {
        float temp=0;
        for(int i=0;i<inputs.size();i++)
        {
            if(gates[i])
            {
                temp++;
            }
        }
        return temp;
    }

    public void draw()
    {
        int i=findMyIndex();
        fill(#ffffaa);
        noStroke();
        rect(140, 20+i*30, 40, 28);
        stroke(#101010);
        for(int q=0;q<gates.length;q++)
        {
            if (gates[q])
            {
                strokeWeight(abs(weights[q])+1);
                line(140, 32+i*30, 56, 20+q*20);
                strokeWeight(1);
            }
        }
        
    }


    private int findMyIndex()
    {
        int temp=0;
        for(int i=0;i<brain.neurons.length;i++)
        {
            if (brain.neurons[i]==this)
            {
                temp=i;
                break;
            }
        }
        return temp;
    }
}