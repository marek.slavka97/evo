class Life
{
    final G global = G.GetG();
    int x, y;
    int tx=0;
    int ty=0;
    int tx2=0;
    int ty2=0;
    int sense=120;
    float beta=random(360);
    float speed=0;
    float energy=200;
    int survival=0;
    boolean activated=false;
    Brain brain=new Brain();
    public Life(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public Life copy()
    {
        reset();
        Life copy=new Life(300,300);
        copy.brain=brain.copy();
        return copy;
    }

    public void nullOutputs()
    {
        brain.nullOutputs();
    }

    public void reset()
    {
        survival=0;
        x=(int)random(600);
        y=(int)random(600);
        tx=0;
        ty=0;
        tx2=0;
        ty2=0;
        energy=400;
        nullOutputs();
    }

    public void step()
    {
        if(activated)
        {
        /// GIVE INFO TO BRAIN
        processFood();
        brain.outputs.get(3).value=/*energy/20*/0;
        brain.outputs.get(4).value=random(1)-0.5;
        brain.outputs.get(5).value=1;
        /// BRAIN CALCS
        brain.step();
        /// GET BEHAVOIR FROM BRAIN
        beta+=brain.outputs.get(11).value*60;
        speed=brain.outputs.get(12).value*5;
        /// FIX BEHAVOIR
        while (beta>180) beta=beta-360;
        while (beta<-180) beta=beta+360;
        /// MOVE
        x+=cos(beta/180*PI)*speed*2.5;
        y-=sin(beta/180*PI)*speed*2.5;
        /// FIX MOVEMENT
        x=max(x,0);
        x=min(x,600);
        y=max(y,0);
        y=min(y,600);
        /// CHECK FOOD
        checkFood();
        /// PAY ENERGY
        energy-=/*(1.0/2.0)*speedabs(speed/4)+*/10/*+brain.getEnergy()/40*/;
        if(energy<=0)activated=false;
        /// COUNT SURVIVAL
        survival++;
        }
    }

    public void draw()
    {
        if(activated)
        {
        fill(#900000);
        noStroke();
        if(global.Lifes.indexOf(this)==0)stroke(#ffffaa);
        ellipse(x + global.mapX, y + global.mapY, 15, 15);
        stroke(#000000);
        line(x + global.mapX, y + global.mapY, x + global.mapX + cos(beta/180*PI)*8, y + global.mapY - sin(beta/180*PI)*8);
        
        //text(beta, x + global.mapX, y + global.mapY);
        fill(#ffff00);
        text((int)energy, x + global.mapX-14, y + global.mapY-10);
        text(global.Lifes.indexOf(this),x + global.mapX-8, y + global.mapY+20);
        fill(#000000);
        if(tx!=0 && ty!=0)line(global.mapX+x, global.mapY+y,global.mapX+tx, global.mapY+ty);
        if(tx2!=0 && ty2!=0)line(global.mapX+x, global.mapY+y,global.mapX+tx2, global.mapY+ty2);
        noFill();
        //ellipse(x + global.mapX, y + global.mapY, 140, 140);
        
        }
        if(global.Lifes.indexOf(this)==0)brain.draw();
        drawSurvival();
    }

    public void processFood()
    {
        Vegetable nearest=null;
        Vegetable nearest2=null;
        float dist=1000;
        float dist2=1000;
        float tempdist=0;
        float alpha=0;
        for(Vegetable v:global.Veggies)
        {
            if(!v.eaten)
            {
                tempdist=sqrt((x-v.x)*(x-v.x)+(y-v.y)*(y-v.y));
                if(tempdist<dist)
                {
                    dist2=dist;
                    dist=tempdist;
                    tx2=tx;
                    ty2=ty;
                    tx=v.x;
                    ty=v.y;
                    nearest2=nearest;
                    nearest=v;
                }
            }
        }
        if(dist<sense/2)
        {
            if(nearest.good)
            brain.outputs.get(0).value=1;
            else
            brain.outputs.get(0).value=-1;

            alpha=-atan2((cos(beta/180*PI)*5)*(ty-y)-(-sin(beta/180*PI)*5)*(tx-x),(cos(beta/180*PI)*5)*(tx-x)+(-sin(beta/180*PI)*5)*(ty-y));
            brain.outputs.get(1).value=alpha/PI;
            brain.outputs.get(2).value=dist/40;
            brain.outputs.get(6).value=nearest.size;
        }
        else
        {
            brain.outputs.get(0).value=0;
            brain.outputs.get(1).value=0;
            brain.outputs.get(2).value=0;
            brain.outputs.get(6).value=0;
            tx=0;
            ty=0;
        }
        if(dist2<sense/2)
        {
            if(nearest2.good)
            brain.outputs.get(7).value=1;
            else
            brain.outputs.get(7).value=-1;

            alpha=-atan2((cos(beta/180*PI)*5)*(ty2-y)-(-sin(beta/180*PI)*5)*(tx2-x),(cos(beta/180*PI)*5)*(tx2-x)+(-sin(beta/180*PI)*5)*(ty2-y));
            brain.outputs.get(8).value=alpha/PI;
            brain.outputs.get(9).value=dist2/40;
            brain.outputs.get(10).value=nearest2.size;
        }
        else
        {
            brain.outputs.get(7).value=0;
            brain.outputs.get(8).value=0;
            brain.outputs.get(9).value=0;
            brain.outputs.get(10).value=0;
            tx2=0;
            ty2=0;
        }
    }

    public void checkFood()
    {
        boolean ate=false;
        float dist;
        for(Vegetable v:global.Veggies)
        {
            if(!v.eaten)
            {
                dist=sqrt((x-v.x)*(x-v.x)+(y-v.y)*(y-v.y));
                if(dist<v.size*2+8)
                {
                    if(survival>1 && v.time>1)
                    {
                    if(v.good)
                    energy+=v.size*v.size*40;
                    else
                    energy-=v.size*v.size*100;
                    }
                    v.eaten=true;
                    ate=true;
                }
            }
        }
        if(ate)
        {
            for(int i=Veggies.size()-1;i>=0;i--)
            {
                if(Veggies.get(i).eaten)
                { 
                    boolean good=Veggies.get(i).good;
                    Veggies.remove(Veggies.get(i));
                    if(random(1)>0.7)
                    {
                        Vegetable tempVeg=new Vegetable();
                        tempVeg.good=good;
                        Veggies.add(tempVeg);
                    }
                }
            }
        }
    }

    public void drawSurvival()
    {
        int index=global.Lifes.indexOf(this);
        fill(#000000);
        if(!activated)fill(#505050);
        text(index+". "+survival, 810, 20+index*18);
        text(brain.neurons.length+" | "+(int)brain.getEnergy(), 880, 20+index*18);
    }

    public int compareTo(Life life) 
    { 
        return survival - life.survival; // Supposes rank() isn't big 
    } 
}