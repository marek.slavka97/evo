class Vegetable
{
    final G global = G.GetG();
    int x, y;
    float size;
    boolean good=true;
    boolean eaten=false;
    int time=0;
    boolean grow=false, grown=false;
    public Vegetable()
    {
        this.x = (int)random(600);
        this.y = (int)random(600);
        good=random(1)>0.2;
        size=1+random(2);
    }
    public Vegetable(int x, int y)
    {
        this.x = x;
        this.y = y;
        size=1+random(2);
    }

    public void step()
    {
        size+=(2-size)/50;
        if(time==70-(int)random(20))
        {
            grow=true;
        }
        time++;
    }

    public void checkGrow()
    {
        if(grow && global.Veggies.size()<150 && !grown)
        {
            grow=false;
            grown=true;
            Vegetable tempVeg=new Vegetable();
            tempVeg.x=x+(int)random(10)-5;
            tempVeg.y=y+(int)random(10)-5;
            tempVeg.good=good;
            tempVeg.size=0.4;
            global.Veggies.add(tempVeg);
        }
    }

    public void draw()
    {
        if(!eaten)
        {
            if(good)
            fill(#009000);
            else
            fill(#902000);
            ellipse(x + global.mapX, y + global.mapY, size*4, size*4);
        }
    }
}