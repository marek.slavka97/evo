class Brain
{
    final G global = G.GetG();
    int senses = 11; //[0=val; 1=deg; 2=dis]; 3=ene; 4=rng; 5=neu; [6=fsize] [7=val; 8=deg; 9=dis; 10=fsize]
    ArrayList<Output> outputs;
    int abilities = 2; //11=turning; 12=speed
    Neuron[] neurons;

    public Brain()
    {
        outputs = new ArrayList<Output>();
        for(int i=0;i<senses;i++)outputs.add(new Output(this));
        neurons = new Neuron[abilities];
        for(int i=0;i<abilities;i++)neurons[i] = new Neuron(this,outputs);
        for(int i=0;i<neurons.length;i++)neurons[i].initGates();
    }
    
    public Brain(Brain copyOf,int abil)
    {
        abilities=abil;
        while(random(1)<0.005)abilities++;
        while(random(1)<0.005)abilities--;
        if(abilities<2)abilities=2;
        outputs = new ArrayList<Output>();
        for(int i=0;i<senses;i++)outputs.add(new Output(this));
        neurons = new Neuron[abilities];
        for(int i=0;i<abilities;i++)
        {
            if(i>copyOf.neurons.length-1) neurons[i] = new Neuron(this,outputs);
            else neurons[i] = copyOf.neurons[i].copy(this,outputs);
        }
        for(int i=0;i<neurons.length;i++) neurons[i].initGates();
        if(copyOf.neurons.length<=neurons.length)
        for(int i=0;i<copyOf.neurons.length;i++) neurons[i].copyGates(copyOf.neurons[i]);
        else
        for(int i=0;i<neurons.length;i++) neurons[i].copyGates(copyOf.neurons[i]);
    }

    public Brain copy()
    {
        Brain copy=new Brain(this,abilities);
        return copy;
    }

    public void nullOutputs()
    {
        for(Output o: outputs)o.value=0;
    }

    public void step()
    {
        for(Neuron n : neurons) n.step();
    }

    public void draw()
    {
        for(Output o : outputs) o.draw();
        for(Neuron n : neurons) n.draw();
        fill(#000000);
        text(getEnergy(), 10, 400);
    }

    public float getEnergy()
    {
        float temp = 0;
        //temp+=(float)neurons.length;
        for(Neuron n : neurons) temp+=n.getGates();
        //for(Neuron n:neurons) temp+=(1+max(n.getGates()-2,0)*max(n.getGates()-2,0));
        return temp;
    }
}