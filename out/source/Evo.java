import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class evo extends PApplet {

//Author: Alialun (@AliFox at Git)
// EVO - Evolution simulator with mash Neural network

final G global = G.GetG();
ArrayList<Vegetable> Veggies = global.Veggies;
ArrayList<Life> Lifes = global.Lifes;
int generation=1;
float year=0;
int subjects=32;
int foods=100;

public void setup() 
{
  size(1000, 800/*, OPENGL*/); 
  frameRate(60);
  for(int i=0;i<foods;i++)Veggies.add(new Vegetable());
  for(int i=0;i<subjects;i++)Lifes.add(new Life(300,300));
}

public void draw()
{
  background(0xffbbbbbb);
  noStroke();
  fill(0xff202080);
  rect(global.mapX, global.mapY, 600, 600);
  fill(0xff000000);
  text("GENERATION "+generation, 450, 640);
  text("YEAR "+(round(year*10)/10.0f), 470, 660);
  for(Vegetable v:Veggies)v.draw();
  for(Life l:Lifes)l.draw();
}

public void keyPressed() {
  if (key == 's') {
    for(Life l:Lifes)l.step();
    for(Vegetable v:Veggies)v.step();
    for(int p=Veggies.size()-1;p>=0;p--)Veggies.get(p).checkGrow();
  }
  if (key == 'w') {
      Veggies.clear();
      for(int q=0;q<foods;q++)Veggies.add(new Vegetable());
      for(int i=0;i<Lifes.size();i++)
      {
        Lifes.get(i).reset();
        Lifes.get(i).activated=true;
      }
      boolean lives=true;
      while(lives){
        lives=false;
        for(Life l:Lifes)if(l.activated)lives=true;
        for(Life l:Lifes)l.step();
        for(Vegetable v:Veggies)v.step();
        for(int p=Veggies.size()-1;p>=0;p--)Veggies.get(p).checkGrow();
      }
    bubbleSort();
  }
  if(key == 'd')
  {
    for(int i=0;i<Lifes.size();i++)
      {
      Lifes.get(i).reset();
      Lifes.get(i).activated=true;
      }
    Veggies.clear();
    for(int q=0;q<foods;q++)Veggies.add(new Vegetable());
  }
  if(key == 'x')
  {
    Lifes.get(0).reset();
    Lifes.get(0).activated=true;
    Veggies.clear();
    for(int q=0;q<foods;q++)Veggies.add(new Vegetable());
  }
  if(key == 'a')
  {
    generation++;
    year+=Lifes.get(0).survival/100.0f;
    ArrayList<Life> temp = new ArrayList<Life>();
    for(int g=0;g<subjects/2;g++)
    {
      temp.add(Lifes.get(g).copy());
      temp.add(Lifes.get(g).copy());
    }
    for(int q=Lifes.size()-1;q>=0;q--)Lifes.remove(q);
    for(Life l:temp)Lifes.add(l);
    Veggies.clear();
    for(int q=0;q<foods;q++)Veggies.add(new Vegetable());
  }
  if(key=='e')
  {
    for(int v=0;v<200;v++)
    {
      Veggies.clear();
      for(int q=0;q<foods;q++)Veggies.add(new Vegetable());
      for(int i=0;i<Lifes.size();i++)
      {
      Lifes.get(i).activated=true;
      }
      boolean lives=true;
      while(lives){
        lives=false;
        for(Life l:Lifes)if(l.activated)lives=true;
        for(Life l:Lifes)l.step();
        for(Vegetable veg:Veggies)veg.step();
        for(int p=Veggies.size()-1;p>=0;p--)Veggies.get(p).checkGrow();
      }
    bubbleSort();
    generation++;
    year+=Lifes.get(0).survival/100.0f;
    ArrayList<Life> temp = new ArrayList<Life>();
    for(int g=0;g<subjects/2;g++)
    {
      temp.add(Lifes.get(g).copy());
      temp.add(Lifes.get(g).copy());
    }
    for(int q=Lifes.size()-1;q>=0;q--)Lifes.remove(q);
    for(Life l:temp)Lifes.add(l);
    Veggies.clear();
    for(int q=0;q<foods;q++)Veggies.add(new Vegetable());
    }
  }
  if(key=='q')
  {
    for(int v=0;v<1000;v++)
    {
      Veggies.clear();
      for(int q=0;q<foods;q++)Veggies.add(new Vegetable());
      for(int i=0;i<Lifes.size();i++)
      {
      Lifes.get(i).activated=true;
      }
      boolean lives=true;
      while(lives){
        lives=false;
        for(Life l:Lifes)if(l.activated)lives=true;
        for(Life l:Lifes)l.step();
        for(Vegetable veg:Veggies)veg.step();
        for(int p=Veggies.size()-1;p>=0;p--)Veggies.get(p).checkGrow();
      }
    bubbleSort();
    generation++;
    year+=Lifes.get(0).survival/100.0f;
    ArrayList<Life> temp = new ArrayList<Life>();
    for(int g=0;g<subjects/4;g++)
    {
      temp.add(Lifes.get(g).copy());
      temp.add(Lifes.get(g).copy());
      temp.add(Lifes.get(g).copy());
      temp.add(Lifes.get(g).copy());
    }
    for(int q=Lifes.size()-1;q>=0;q--)Lifes.remove(q);
    for(Life l:temp)Lifes.add(l);
    Veggies.clear();
    for(int q=0;q<foods;q++)Veggies.add(new Vegetable());
    }
  }
}

public void bubbleSort()
{
  Life tempj,tempi;
  for (int i = 0; i < global.Lifes.size() - 1; i ++) {
    for (int j = i + 1; j < global.Lifes.size(); j ++) {
      if (global.Lifes.get(i).survival < global.Lifes.get(j).survival) {
        tempj = global.Lifes.get(j);
        tempi = global.Lifes.get(i);
        Lifes.remove(tempj);
        Lifes.add(i,tempj);
      }
    }
  }
}
class Brain
{
    final G global = G.GetG();
    int senses = 11; //[0=val; 1=deg; 2=dis]; 3=ene; 4=rng; 5=neu; [6=fsize] [7=val; 8=deg; 9=dis; 10=fsize]
    ArrayList<Output> outputs;
    int abilities = 2; //11=turning; 12=speed
    Neuron[] neurons;

    public Brain()
    {
        outputs = new ArrayList<Output>();
        for(int i=0;i<senses;i++)outputs.add(new Output(this));
        neurons = new Neuron[abilities];
        for(int i=0;i<abilities;i++)neurons[i] = new Neuron(this,outputs);
        for(int i=0;i<neurons.length;i++)neurons[i].initGates();
    }
    
    public Brain(Brain copyOf,int abil)
    {
        abilities=abil;
        while(random(1)<0.005f)abilities++;
        while(random(1)<0.005f)abilities--;
        if(abilities<2)abilities=2;
        outputs = new ArrayList<Output>();
        for(int i=0;i<senses;i++)outputs.add(new Output(this));
        neurons = new Neuron[abilities];
        for(int i=0;i<abilities;i++)
        {
            if(i>copyOf.neurons.length-1) neurons[i] = new Neuron(this,outputs);
            else neurons[i] = copyOf.neurons[i].copy(this,outputs);
        }
        for(int i=0;i<neurons.length;i++) neurons[i].initGates();
        if(copyOf.neurons.length<=neurons.length)
        for(int i=0;i<copyOf.neurons.length;i++) neurons[i].copyGates(copyOf.neurons[i]);
        else
        for(int i=0;i<neurons.length;i++) neurons[i].copyGates(copyOf.neurons[i]);
    }

    public Brain copy()
    {
        Brain copy=new Brain(this,abilities);
        return copy;
    }

    public void nullOutputs()
    {
        for(Output o: outputs)o.value=0;
    }

    public void step()
    {
        for(Neuron n : neurons) n.step();
    }

    public void draw()
    {
        for(Output o : outputs) o.draw();
        for(Neuron n : neurons) n.draw();
        fill(0xff000000);
        text(getEnergy(), 10, 400);
    }

    public float getEnergy()
    {
        float temp = 0;
        //temp+=(float)neurons.length;
        for(Neuron n : neurons) temp+=n.getGates();
        //for(Neuron n:neurons) temp+=(1+max(n.getGates()-2,0)*max(n.getGates()-2,0));
        return temp;
    }
}
public static class G
{
    private static G globals;

    //=// GLOBALS HERE //=// (G.GetG() to get them) //=//
    public int mapX = 200, mapY = 10;
    public ArrayList<Vegetable> Veggies = new ArrayList<Vegetable>();
    public ArrayList<Life> Lifes = new ArrayList<Life>();
    ////////////////////////////////////////////////////

    private G(){
    }

    public static G GetG(){
        if(globals == null){
            globals = new G();
        }
        return globals;
    }
}
class Life
{
    final G global = G.GetG();
    int x, y;
    int tx=0;
    int ty=0;
    int tx2=0;
    int ty2=0;
    int sense=120;
    float beta=random(360);
    float speed=0;
    float energy=200;
    int survival=0;
    boolean activated=false;
    Brain brain=new Brain();
    public Life(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public Life copy()
    {
        reset();
        Life copy=new Life(300,300);
        copy.brain=brain.copy();
        return copy;
    }

    public void nullOutputs()
    {
        brain.nullOutputs();
    }

    public void reset()
    {
        survival=0;
        x=(int)random(600);
        y=(int)random(600);
        tx=0;
        ty=0;
        tx2=0;
        ty2=0;
        energy=400;
        nullOutputs();
    }

    public void step()
    {
        if(activated)
        {
        /// GIVE INFO TO BRAIN
        processFood();
        brain.outputs.get(3).value=/*energy/20*/0;
        brain.outputs.get(4).value=random(1)-0.5f;
        brain.outputs.get(5).value=1;
        /// BRAIN CALCS
        brain.step();
        /// GET BEHAVOIR FROM BRAIN
        beta+=brain.outputs.get(11).value*60;
        speed=brain.outputs.get(12).value*5;
        /// FIX BEHAVOIR
        while (beta>180) beta=beta-360;
        while (beta<-180) beta=beta+360;
        /// MOVE
        x+=cos(beta/180*PI)*speed*2.5f;
        y-=sin(beta/180*PI)*speed*2.5f;
        /// FIX MOVEMENT
        x=max(x,0);
        x=min(x,600);
        y=max(y,0);
        y=min(y,600);
        /// CHECK FOOD
        checkFood();
        /// PAY ENERGY
        energy-=/*(1.0/2.0)*speedabs(speed/4)+*/10/*+brain.getEnergy()/40*/;
        if(energy<=0)activated=false;
        /// COUNT SURVIVAL
        survival++;
        }
    }

    public void draw()
    {
        if(activated)
        {
        fill(0xff900000);
        noStroke();
        if(global.Lifes.indexOf(this)==0)stroke(0xffffffaa);
        ellipse(x + global.mapX, y + global.mapY, 15, 15);
        stroke(0xff000000);
        line(x + global.mapX, y + global.mapY, x + global.mapX + cos(beta/180*PI)*8, y + global.mapY - sin(beta/180*PI)*8);
        
        //text(beta, x + global.mapX, y + global.mapY);
        fill(0xffffff00);
        text((int)energy, x + global.mapX-14, y + global.mapY-10);
        text(global.Lifes.indexOf(this),x + global.mapX-8, y + global.mapY+20);
        fill(0xff000000);
        if(tx!=0 && ty!=0)line(global.mapX+x, global.mapY+y,global.mapX+tx, global.mapY+ty);
        if(tx2!=0 && ty2!=0)line(global.mapX+x, global.mapY+y,global.mapX+tx2, global.mapY+ty2);
        noFill();
        //ellipse(x + global.mapX, y + global.mapY, 140, 140);
        
        }
        if(global.Lifes.indexOf(this)==0)brain.draw();
        drawSurvival();
    }

    public void processFood()
    {
        Vegetable nearest=null;
        Vegetable nearest2=null;
        float dist=1000;
        float dist2=1000;
        float tempdist=0;
        float alpha=0;
        for(Vegetable v:global.Veggies)
        {
            if(!v.eaten)
            {
                tempdist=sqrt((x-v.x)*(x-v.x)+(y-v.y)*(y-v.y));
                if(tempdist<dist)
                {
                    dist2=dist;
                    dist=tempdist;
                    tx2=tx;
                    ty2=ty;
                    tx=v.x;
                    ty=v.y;
                    nearest2=nearest;
                    nearest=v;
                }
            }
        }
        if(dist<sense/2)
        {
            if(nearest.good)
            brain.outputs.get(0).value=1;
            else
            brain.outputs.get(0).value=-1;

            alpha=-atan2((cos(beta/180*PI)*5)*(ty-y)-(-sin(beta/180*PI)*5)*(tx-x),(cos(beta/180*PI)*5)*(tx-x)+(-sin(beta/180*PI)*5)*(ty-y));
            brain.outputs.get(1).value=alpha/PI;
            brain.outputs.get(2).value=dist/40;
            brain.outputs.get(6).value=nearest.size;
        }
        else
        {
            brain.outputs.get(0).value=0;
            brain.outputs.get(1).value=0;
            brain.outputs.get(2).value=0;
            brain.outputs.get(6).value=0;
            tx=0;
            ty=0;
        }
        if(dist2<sense/2)
        {
            if(nearest2.good)
            brain.outputs.get(7).value=1;
            else
            brain.outputs.get(7).value=-1;

            alpha=-atan2((cos(beta/180*PI)*5)*(ty2-y)-(-sin(beta/180*PI)*5)*(tx2-x),(cos(beta/180*PI)*5)*(tx2-x)+(-sin(beta/180*PI)*5)*(ty2-y));
            brain.outputs.get(8).value=alpha/PI;
            brain.outputs.get(9).value=dist2/40;
            brain.outputs.get(10).value=nearest2.size;
        }
        else
        {
            brain.outputs.get(7).value=0;
            brain.outputs.get(8).value=0;
            brain.outputs.get(9).value=0;
            brain.outputs.get(10).value=0;
            tx2=0;
            ty2=0;
        }
    }

    public void checkFood()
    {
        boolean ate=false;
        float dist;
        for(Vegetable v:global.Veggies)
        {
            if(!v.eaten)
            {
                dist=sqrt((x-v.x)*(x-v.x)+(y-v.y)*(y-v.y));
                if(dist<v.size*2+8)
                {
                    if(survival>1 && v.time>1)
                    {
                    if(v.good)
                    energy+=v.size*v.size*40;
                    else
                    energy-=v.size*v.size*100;
                    }
                    v.eaten=true;
                    ate=true;
                }
            }
        }
        if(ate)
        {
            for(int i=Veggies.size()-1;i>=0;i--)
            {
                if(Veggies.get(i).eaten)
                { 
                    boolean good=Veggies.get(i).good;
                    Veggies.remove(Veggies.get(i));
                    if(random(1)>0.7f)
                    {
                        Vegetable tempVeg=new Vegetable();
                        tempVeg.good=good;
                        Veggies.add(tempVeg);
                    }
                }
            }
        }
    }

    public void drawSurvival()
    {
        int index=global.Lifes.indexOf(this);
        fill(0xff000000);
        if(!activated)fill(0xff505050);
        text(index+". "+survival, 810, 20+index*18);
        text(brain.neurons.length+" | "+(int)brain.getEnergy(), 880, 20+index*18);
    }

    public int compareTo(Life life) 
    { 
        return survival - life.survival; // Supposes rank() isn't big 
    } 
}
class Neuron
{
    Brain brain;
    ArrayList<Output> inputs;
    boolean[] gates;
    float[] weights;
    Output output;
    float MOD;
    public Neuron(Brain a, ArrayList<Output> i)
    {
        brain=a;
        inputs=i;
        output=new Output(a);
        inputs.add(output);
    }
    
    public Neuron copy(Brain newBrain, ArrayList<Output> newOutputs)
    {
        Neuron copy=new Neuron(newBrain,newOutputs);
        return copy;
    }

    public void copyGates(Neuron template)
    {
        if(template.gates.length<=gates.length)
        {
        for(int i=0;i<template.gates.length;i++)
            if(random(1)>0.1f)
            {
                gates[i]=template.gates[i];
            }
        for(int i=0;i<template.weights.length;i++)
            weights[i]=template.weights[i]+random(0.2f)-0.1f;
        }
        else
        {
        for(int i=0;i<gates.length;i++)
            if(random(1)>0.05f)
            {
                gates[i]=template.gates[i];
            }
        for(int i=0;i<weights.length;i++)
            weights[i]=template.weights[i]+random(0.2f)-0.1f;
        }
        
        MOD=template.MOD+random(0.2f)-0.1f;
    }

    public void initGates()
    {
        gates=new boolean[inputs.size()];
        for(int i=0;i<gates.length;i++)gates[i]=(random(1) > 0.5f);
        weights=new float[inputs.size()];
        for(int i=0;i<weights.length;i++)weights[i]=random(4)-2;
        MOD=random(4)-2;
    }

    public void step()
    {
        float temp=0;
        for(int i=0;i<inputs.size();i++)
        {
            if(gates[i])
            {
                temp+=inputs.get(i).value*weights[i];
            }
        }
        output.value=atan(temp*MOD)/HALF_PI;
    }

    public float getGates()
    {
        float temp=0;
        for(int i=0;i<inputs.size();i++)
        {
            if(gates[i])
            {
                temp++;
            }
        }
        return temp;
    }

    public void draw()
    {
        int i=findMyIndex();
        fill(0xffffffaa);
        noStroke();
        rect(140, 20+i*30, 40, 28);
        stroke(0xff101010);
        for(int q=0;q<gates.length;q++)
        {
            if (gates[q])
            {
                strokeWeight(abs(weights[q])+1);
                line(140, 32+i*30, 56, 20+q*20);
                strokeWeight(1);
            }
        }
        
    }


    private int findMyIndex()
    {
        int temp=0;
        for(int i=0;i<brain.neurons.length;i++)
        {
            if (brain.neurons[i]==this)
            {
                temp=i;
                break;
            }
        }
        return temp;
    }
}
class Output
{
    public float value=0;
    Brain brain;
    public Output(Brain a)
    {
        brain=a;
    }
    public void draw()
    {
        int i=brain.outputs.indexOf(this);
        fill(0xff000000);
        noStroke();
        ellipse(32, 20+i*20, 50, 18);
        fill(0xffffffff);
        text(value, 10, 25+i*20);
    }

    public void setBrain(Brain a)
    {
        brain=a;
    }
}
class Vegetable
{
    final G global = G.GetG();
    int x, y;
    float size;
    boolean good=true;
    boolean eaten=false;
    int time=0;
    boolean grow=false, grown=false;
    public Vegetable()
    {
        this.x = (int)random(600);
        this.y = (int)random(600);
        good=random(1)>0.2f;
        size=1+random(2);
    }
    public Vegetable(int x, int y)
    {
        this.x = x;
        this.y = y;
        size=1+random(2);
    }

    public void step()
    {
        size+=(2-size)/50;
        if(time==70-(int)random(20))
        {
            grow=true;
        }
        time++;
    }

    public void checkGrow()
    {
        if(grow && global.Veggies.size()<150 && !grown)
        {
            grow=false;
            grown=true;
            Vegetable tempVeg=new Vegetable();
            tempVeg.x=x+(int)random(10)-5;
            tempVeg.y=y+(int)random(10)-5;
            tempVeg.good=good;
            tempVeg.size=0.4f;
            global.Veggies.add(tempVeg);
        }
    }

    public void draw()
    {
        if(!eaten)
        {
            if(good)
            fill(0xff009000);
            else
            fill(0xff902000);
            ellipse(x + global.mapX, y + global.mapY, size*4, size*4);
        }
    }
}
  public void settings() {  size(1000, 800            ); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "evo" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
