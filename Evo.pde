//Author: Alialun (@AliFox at Git)
// EVO - Evolution simulator with mash Neural network

final G global = G.GetG();
ArrayList<Vegetable> Veggies = global.Veggies;
ArrayList<Life> Lifes = global.Lifes;
int generation=1;
float year=0;
int subjects=32;
int foods=100;

void setup() 
{
  size(1000, 800/*, OPENGL*/); 
  frameRate(60);
  for(int i=0;i<foods;i++)Veggies.add(new Vegetable());
  for(int i=0;i<subjects;i++)Lifes.add(new Life(300,300));
}

void draw()
{
  background(#bbbbbb);
  noStroke();
  fill(#202080);
  rect(global.mapX, global.mapY, 600, 600);
  fill(#000000);
  text("GENERATION "+generation, 450, 640);
  text("YEAR "+(round(year*10)/10.0), 470, 660);
  for(Vegetable v:Veggies)v.draw();
  for(Life l:Lifes)l.draw();
}

void keyPressed() {
  if (key == 's') {
    for(Life l:Lifes)l.step();
    for(Vegetable v:Veggies)v.step();
    for(int p=Veggies.size()-1;p>=0;p--)Veggies.get(p).checkGrow();
  }
  if (key == 'w') {
      Veggies.clear();
      for(int q=0;q<foods;q++)Veggies.add(new Vegetable());
      for(int i=0;i<Lifes.size();i++)
      {
        Lifes.get(i).reset();
        Lifes.get(i).activated=true;
      }
      boolean lives=true;
      while(lives){
        lives=false;
        for(Life l:Lifes)if(l.activated)lives=true;
        for(Life l:Lifes)l.step();
        for(Vegetable v:Veggies)v.step();
        for(int p=Veggies.size()-1;p>=0;p--)Veggies.get(p).checkGrow();
      }
    bubbleSort();
  }
  if(key == 'd')
  {
    for(int i=0;i<Lifes.size();i++)
      {
      Lifes.get(i).reset();
      Lifes.get(i).activated=true;
      }
    Veggies.clear();
    for(int q=0;q<foods;q++)Veggies.add(new Vegetable());
  }
  if(key == 'x')
  {
    Lifes.get(0).reset();
    Lifes.get(0).activated=true;
    Veggies.clear();
    for(int q=0;q<foods;q++)Veggies.add(new Vegetable());
  }
  if(key == 'a')
  {
    generation++;
    year+=Lifes.get(0).survival/100.0;
    ArrayList<Life> temp = new ArrayList<Life>();
    for(int g=0;g<subjects/2;g++)
    {
      temp.add(Lifes.get(g).copy());
      temp.add(Lifes.get(g).copy());
    }
    for(int q=Lifes.size()-1;q>=0;q--)Lifes.remove(q);
    for(Life l:temp)Lifes.add(l);
    Veggies.clear();
    for(int q=0;q<foods;q++)Veggies.add(new Vegetable());
  }
  if(key=='e')
  {
    for(int v=0;v<200;v++)
    {
      Veggies.clear();
      for(int q=0;q<foods;q++)Veggies.add(new Vegetable());
      for(int i=0;i<Lifes.size();i++)
      {
      Lifes.get(i).activated=true;
      }
      boolean lives=true;
      while(lives){
        lives=false;
        for(Life l:Lifes)if(l.activated)lives=true;
        for(Life l:Lifes)l.step();
        for(Vegetable veg:Veggies)veg.step();
        for(int p=Veggies.size()-1;p>=0;p--)Veggies.get(p).checkGrow();
      }
    bubbleSort();
    generation++;
    year+=Lifes.get(0).survival/100.0;
    ArrayList<Life> temp = new ArrayList<Life>();
    for(int g=0;g<subjects/2;g++)
    {
      temp.add(Lifes.get(g).copy());
      temp.add(Lifes.get(g).copy());
    }
    for(int q=Lifes.size()-1;q>=0;q--)Lifes.remove(q);
    for(Life l:temp)Lifes.add(l);
    Veggies.clear();
    for(int q=0;q<foods;q++)Veggies.add(new Vegetable());
    }
  }
  if(key=='q')
  {
    for(int v=0;v<1000;v++)
    {
      Veggies.clear();
      for(int q=0;q<foods;q++)Veggies.add(new Vegetable());
      for(int i=0;i<Lifes.size();i++)
      {
      Lifes.get(i).activated=true;
      }
      boolean lives=true;
      while(lives){
        lives=false;
        for(Life l:Lifes)if(l.activated)lives=true;
        for(Life l:Lifes)l.step();
        for(Vegetable veg:Veggies)veg.step();
        for(int p=Veggies.size()-1;p>=0;p--)Veggies.get(p).checkGrow();
      }
    bubbleSort();
    generation++;
    year+=Lifes.get(0).survival/100.0;
    ArrayList<Life> temp = new ArrayList<Life>();
    for(int g=0;g<subjects/4;g++)
    {
      temp.add(Lifes.get(g).copy());
      temp.add(Lifes.get(g).copy());
      temp.add(Lifes.get(g).copy());
      temp.add(Lifes.get(g).copy());
    }
    for(int q=Lifes.size()-1;q>=0;q--)Lifes.remove(q);
    for(Life l:temp)Lifes.add(l);
    Veggies.clear();
    for(int q=0;q<foods;q++)Veggies.add(new Vegetable());
    }
  }
}

void bubbleSort()
{
  Life tempj,tempi;
  for (int i = 0; i < global.Lifes.size() - 1; i ++) {
    for (int j = i + 1; j < global.Lifes.size(); j ++) {
      if (global.Lifes.get(i).survival < global.Lifes.get(j).survival) {
        tempj = global.Lifes.get(j);
        tempi = global.Lifes.get(i);
        Lifes.remove(tempj);
        Lifes.add(i,tempj);
      }
    }
  }
}