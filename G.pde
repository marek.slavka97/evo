public static class G
{
    private static G globals;

    //=// GLOBALS HERE //=// (G.GetG() to get them) //=//
    public int mapX = 200, mapY = 10;
    public ArrayList<Vegetable> Veggies = new ArrayList<Vegetable>();
    public ArrayList<Life> Lifes = new ArrayList<Life>();
    ////////////////////////////////////////////////////

    private G(){
    }

    public static G GetG(){
        if(globals == null){
            globals = new G();
        }
        return globals;
    }
}